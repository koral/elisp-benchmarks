;;; elisp-benchmark.el ---  -*- lexical-binding:t -*-

;; Copyright (C) 2019 Andrea Corallo

;; Maintainer: akrl@sdf.org
;; Package: elisp-benchmark
;; Homepage: https://gitlab.com/koral/elisp-benchmark
;; Version: 0.2
;; Package-Requires: ((emacs "27") (cl-lib "0.5"))
;; Keywords: languages, extensions, verilog, hardware, rtl

;; This file is not part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;; In use for testing the Emacs Lisp native compiler performance.

;; Note: micro benchmarks are trimmed to run byte-compiled ~30 secs
;; while non micro ones ~100 secs.

;;; Usage:
;; emacs -batch -l ~/elisp-benchmark/elisp-benchmark.el --eval '(elb-run)'

;;; Code:

(require 'cl-lib)
(require 'comp)

(defconst elb-runs 3)
(defconst elb-speed 3)

(defconst elb-bench-folder
  (concat (file-name-directory (or load-file-name buffer-file-name))
	  "benchmarks/"))

(defconst elb-benchmarks '(;; pidigits is not worth to running cause
			   ;; is essentially a bignum bound benchmark.
			   ;; ("pidigits.el" . elb-pidigits-entry)
			   ("nbody.el". elb-nbody-entry)
			   ("bubble.el" . elb-bubble-entry)
			   ("bubble-no-cons.el" . elb-bubble-no-cons-entry)
			   ("fibn.el" . elb-fibn-entry)
			   ("fibn-rec.el" . elb-fibn-rec-entry)
			   ("fibn-tc.el" . elb-fibn-tc-entry)
			   ("inclist-tc.el" . elb-inclist-tc-entry)
			   ("listlen-tc.el" . elb-listlen-tc-entry)
			   ("inclist-no-type-hints.el" . elb-inclist-no-type-hints-entry)
			   ("inclist-type-hints.el" . elb-inclist-type-hints-entry)
			   ("dhrystone.el" . dhrystone-entry)))

(defmacro elb-time (&rest body)
  "Measure and return the time it takes to evaluate BODY."
  (declare (doc-string 2) (indent 1))
  (let ((t-sym (gensym)))
    `(let ((,t-sym (current-time)))
       ,@body
       (float-time (time-since ,t-sym)))))

(defun elb-run-byte-compiled (file entry-point)
  (garbage-collect)
  (cl-assert (and (load (concat file "c"))))
  (message "Running %s byte compiled..." file)
  (elb-time
   (message "res: %s" (funcall entry-point))))

(defun elb-run-native-compiled (file entry-point)
  (garbage-collect)
  (cl-assert
   (and (load (concat file "n"))
	(subr-native-elisp-p (symbol-function entry-point))))
  (message "Running %s native compiled..." file)
  (elb-time
   (message "res: %s" (funcall entry-point))))

(defun elb-run ()
  "Run all the benchmarks."
  (let ((res (make-hash-table :test #'equal)))
    (cl-loop with comp-speed = elb-speed
	     for (file . _) in elb-benchmarks
	     for abs-file = (concat elb-bench-folder file)
	     do (puthash file () res)
	        (message " Compiling %s" file)
	        (native-compile abs-file)
	        (byte-compile-file abs-file))
    (cl-loop
     repeat elb-runs
     do (cl-loop for (file . entry-point) in elb-benchmarks
		 for abs-file = (concat elb-bench-folder file)
		 for byte-t = (elb-run-byte-compiled abs-file entry-point)
		 for native-t = (elb-run-native-compiled abs-file entry-point)
		 for up-lift = (* 100 (1- (/ byte-t native-t)))
		 do (message "%s orig time: %fs native time: %fs boost %f%%" file
			     byte-t native-t up-lift)
		    (push (list byte-t native-t) (gethash file res))))
    (message "Summary results:")
    (let ((byte-tot 0)
	  (native-tot 0))
      (maphash (lambda (test values)
		 (cl-loop
		  for (byte native) in values
		  sum byte into byte-t
		  sum native into native-t
		  finally
		    (cl-incf byte-tot byte)
		    (cl-incf native-tot native)
		    (message "%s byte time: %fs native time: %fs boost %f%%" test
		    	     byte-t native-t (* 100 (1- (/ byte-t native-t))))))
	       res)
      (message "Total byte time: %fs native time: %fs boost %f%%"
	       byte-tot native-tot (* 100 (1- (/ byte-tot native-tot)))))))

;;; elisp-benchmark.el ends here
