;; -*- lexical-binding: t; -*-
;; From:
;; https://www.emacswiki.org/emacs/EmacsLispBenchmark

(require 'cl-lib)

(defvar elb-bubble-len 1000)
(defvar elb-bubble-list (mapcar #'random (make-list elb-bubble-len
						    most-positive-fixnum)))

(defun elb-bubble (list)
  (let ((i (length list)))
    (while (> i 1)
      (let ((b list))
        (while (cdr b)
          (when (< (cadr b) (car b))
            (setcar b (prog1 (cadr b)
                        (setcdr b (cons (car b) (cddr b))))))
          (setq b (cdr b))))
      (setq i (1- i)))
    list))

(defun elb-bubble-entry ()
  (cl-loop repeat 100
	   for l = (copy-sequence elb-bubble-list)
	   do (elb-bubble l)))

(provide 'elb-bubble)
