;; -*- lexical-binding: t; -*-
;; This micro-benchmark is to compare against inclist-type-hints.el
(require 'cl-lib)

(defvar elb-inclist-no-type-hints-len 50000)
(defvar elb-inclist-no-type-hints-list (mapcar #'random (make-list elb-inclist-no-type-hints-len
								   100)))

(defun elb-inclist-no-th (l)
  (prog1 l
    (while l
      (let ((c l))
	(setcar c (1+ (car c)))
	(setq l (cdr c))))))

(defun elb-inclist-no-type-hints-entry ()
  (let ((l (copy-sequence elb-inclist-no-type-hints-list)))
    (cl-loop repeat 10000
	     do (elb-inclist-no-th l))))
