;; -*- lexical-binding: t; -*-

(require 'cl-lib)

(defun elb-fibn-tc (a b count)
  (if (= count 0)
      b
    (elb-fibn-tc (+ a b) a (- count 1))))

(defun elb-fibn-tc-entry ()
  (cl-loop repeat 1000000
	   do (elb-fibn-tc 1 0 80)))
