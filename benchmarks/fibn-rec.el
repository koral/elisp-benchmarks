;; -*- lexical-binding: t; -*-

(defun elb-fib (n)
  (cond ((= n 0) 0)
	((= n 1) 1)
	(t (+ (elb-fib (- n 1))
	      (elb-fib (- n 2))))))

(defun elb-fibn-rec-entry ()
  (elb-fib 37))
