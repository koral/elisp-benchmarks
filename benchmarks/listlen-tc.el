;; -*- lexical-binding: t; -*-

(require 'cl-lib)

(defvar elb-listlen-tc-len 300)
(defvar elb-listlen-tc-list (mapcar #'random (make-list elb-listlen-tc-len
							100)))

(defun elb-listlen-tc (l n)
  (if (null l)
      n
    (cl-incf n)
    (elb-listlen-tc (cdr l) n)))

(defun elb-listlen-tc-entry ()
  (let ((l (copy-sequence elb-listlen-tc-list)))
    (cl-loop repeat 350000
	     do (elb-listlen-tc l 0))))
