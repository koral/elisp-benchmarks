;; -*- lexical-binding: t; -*-

(require 'cl-lib)

(defvar elb-inclist-type-hints-len 50000)
(defvar elb-inclist-type-hints-list (mapcar #'random (make-list elb-inclist-type-hints-len
								100)))

(defmacro elb-fixnum (x)
  (if (and (boundp comp-native-compiling)
	   comp-native-compiling)
      `(comp-hint-fixnum ,x)
    x))

(defmacro elb-cons (x)
  (if (and (boundp comp-native-compiling)
	   comp-native-compiling)
      `(comp-hint-cons ,x)
    x))

(defun elb-inclist-th (l)
  (prog1 l
    (while l
      (let ((c (elb-cons l)))
	(setcar c (1+ (elb-fixnum (car c))))
	(setq l (cdr c))))))

(defun elb-inclist-type-hints-entry ()
  (let ((l (copy-sequence elb-inclist-type-hints-list)))
    (cl-loop repeat 10000
	     do (elb-inclist-th l))))
