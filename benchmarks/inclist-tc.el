;; -*- lexical-binding: t; -*-

(require 'cl-lib)

(defvar elb-inclist-tc-len 300)
(defvar elb-inclist-tc-list (mapcar #'random (make-list elb-inclist-tc-len
							100)))

(defun elb-inclist-tc (l)
  (when l
    (cl-incf (car l))
    (elb-inclist-tc (cdr l))))

(defun elb-inclist-tc-entry ()
  (let ((l (copy-sequence elb-inclist-tc-list)))
    (cl-loop repeat 350000
	     do (elb-inclist-tc l))))
